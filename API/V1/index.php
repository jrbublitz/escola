<?php

header('Content-Type: application/json; charset=utf-8');

require_once 'Classes/Alunos.php';
require_once 'Classes/Gabarito.php';
require_once 'Classes/Materia.php';
require_once 'Classes/Prova.php';
  
class Rest
{
    public static function open($requisicao)
    {
        $url = explode('/', $requisicao['url']);
        
        $classe = ucfirst($url[0]);
        array_shift($url);
        
        $metodo = $url[0];
        array_shift($url);
        
        $parametros = array(); 
        $parametros = $url;
      
    try {
        if(class_exists($classe)){
            if(method_exists($classe, $metodo)){
                $retorno = call_user_func_array(array(new $classe, $metodo), $parametros);                     

                if($retorno != "erro"){                                          
                    return  json_encode(array('status' => 'Sucesso', 'dados' => $retorno));                                                                                                                                                                        
                }else{                    
                    return json_encode(array('status' => 'Falha', 'dados' => 'Dados Incorretos'));                  
                }
                                
//                $arquivo = fopen('Respostajson.txt','w');
//                fwrite($arquivo, json_encode(array('status' => 'Sucesso', 'dados' => $retorno)));  
//                fclose($arquivo);
                
            } else {
                return json_encode(array('status' => 'Falha', 'dados' => 'Método Inexistente'));
            }
            
        } else {
            return json_encode(array('status' => 'Falha', 'dados' => 'Classe Inexistente'));
        }        
    } catch(Exception $e) {
        return json_encode(array('status' => 'Falha', 'dados' => $e->getMessage()));
    }        
        
    }
}

if(isset($_REQUEST)){
    echo Rest::open($_REQUEST);
  }

?>