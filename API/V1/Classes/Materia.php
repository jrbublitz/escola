<?php
    class Materia{
        
        public function mostrar()
        {
            $con = new PDO('mysql: host=localhost; dbname=mydb;', 'root', '');    
            
            $sql = "SELECT * FROM MATERIA";   
            $sql = $con->prepare($sql);
            $sql->execute(); 

            $resultados = array();

            while($row = $sql->fetch(PDO::FETCH_ASSOC)){
                $resultados[] = $row;                           
            }

            if(!$resultados){
                echo "<h1>Não existem matérias</h1>";
                throw new Exception("Não existem matérias", 1);       
            } 
            
            return $resultados;            
        }

        public function cadastrar()
        {            
            $materia   = $_POST['NomeMateria'];
            $professor = $_POST['NomeProfessor'];               

            if (!($materia == '' or $professor == '')){
                
                $con = new PDO('mysql: host=localhost; dbname=mydb;', 'root', '');

                $sql = "INSERT INTO MATERIA (NomeMateria, NomeProfessor) VALUES ('$materia', '$professor')";   
                $sql = $con->prepare($sql);
                $sql->execute(); 

            }else{
                return "erro";
            }  
        }

    }
?>