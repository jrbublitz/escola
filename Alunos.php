<html>    
    <head>        
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="CSS/Style.css" media="screen" />
    </head>
    <body>
        <div class="container-fluid" id="topoPadrao">
            <h1 id="tituloAluno">Registros dos alunos</h1>
        </div>      
            <div class="container-fluid" id="CorpoAluno">
             <form method="post" action="Prova.php">                    
                 <?php
                    require_once 'API/V1/Classes/Alunos.php'; 

                    $alunos = new Alunos();                        
                    $json_str = json_encode($alunos->mostrar());                      
                    $obj = json_decode($json_str); 
                                              
                    for( $i = 0; $i < count($obj); $i++ ){
                        $matricula[$i] = $obj[$i];               
                    }

                     foreach( $matricula as $e ){
                        echo "<strong>Matrícula: </strong>$e->Matricula<br><br>";
                        echo "<Strong>Nome: </Strong>$e->Nome<br><br>";     
                        echo "<button id='btnPadrao' name='matricula' value='$e->Matricula'>Questões</button> ";                                           
                        echo "<hr>";                     
                    }     
                ?>
             </form>                   
            <div class="container-fluid">        
                <div class="col-sm-6">
                    <form style="float:right" method="post" action="api/V1/Alunos/mostrar">                
                        <input id="btnJson" type="submit" value="Mostrar JSON">
                    </form>
                </div>

                <div class="col-sm-6">
                    <form style="float:left" method="post" action="Aprovados.php">                
                        <input id="btnAprovados" type="submit" value="Aprovados">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>