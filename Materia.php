<html>    
    <head>        
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="CSS/Style.css" media="screen" />
    </head>
    <body>
        <div class="container-fluid" id="topo">
            <h1 id="titulo">Registros das matérias</h1>          
        </div>
        <div class="container-fluid" id="corpoMateria">
             <form method="post" action="">                                    
             <?php
                    require_once 'API/V1/Classes/Materia.php'; 

                    $materias = new Materia();                        
                    $json_str = json_encode($materias->mostrar());                      
                    $obj = json_decode($json_str);         

                    for( $i = 0; $i < count($obj); $i++ ){
                        $materia[$i] = $obj[$i];               
                    }

                     foreach( $materia as $e ){
                        echo "<strong>Professor: </strong>$e->NomeProfessor<br><br>";
                        echo "<Strong>Matéria: </Strong>$e->NomeMateria<br><br>";                             
                        echo "<hr>";                     
                    }     
                ?>
             </form>            
            <form method="post" action="api/V1/Materia/mostrar">                
                <input id="btnJson" type="submit" value="Mostrar JSON">
            </form>
        </div>
    </body>
</html>