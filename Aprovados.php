<html>    
    <head> 
        <meta charset="ISO-8859-1">       
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="CSS/Style.css" media="screen" />
    </head>
    <body>
        <div class="container-fluid" id="topoPadrao">
            <h1 id="tituloAluno">Registros dos alunos Aprovados</h1>
        </div>      
            <div class="container-fluid" id="CorpoAluno">
             <form method="post" action=""> 
             <?php
                 $con = new PDO('mysql: host=localhost; dbname=mydb;', 'root', ''); 
        
                 $sql = "select * from prova a 
                        inner join aluno b on a.FKAluno = b.Matricula 
                        inner join materia c on a.FKMateria = c.PKMATERIA
                        inner join gabarito d on c.PKMATERIA = d.FKMateria
                        where a.Situacao = 'aprovado'";
                 $sql = $con->prepare($sql);
                 $sql->execute();
                 
                 $resultados = array();
                 
                 while($row = $sql->fetch(PDO::FETCH_ASSOC)){
                     $resultados[] = $row;                           
                 }
                 
                 if(!$resultados){
                     echo "<h1>Não existem alunos aprovados</h1>";
                     throw new Exception("Aluno não encontrado", 1);       
                 }   
                 
                 $json_str = json_encode($resultados);
                 $obj = json_decode($json_str);
                 
                 for( $i = 0; $i < count($obj); $i++ ){
                     $aluno[$i] = $obj[$i];               
                 } 

                 foreach ( $aluno as $e ){
                    echo "<strong>Matrícula: </strong>$e->Matricula<br><br>";
                    echo "<Strong>Nome: </Strong>$e->Nome<br><br>";   
                    echo "<button id='btnAprovados1' name='matricula'>Aprovado - $e->Descricao</button> ";  
                    echo "<hr>";     
                }     
             ?>                                
             </form>                              
        </div>
    </body>
</html>