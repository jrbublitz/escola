<html>    
    <head>        
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="CSS/Style.css" media="screen"/>
    </head>
    
    <body>
        <div class="container-fluid" id="topo">
            <h1 id="titulo">Registro de Gabaritos</h1>
        </div>
        <div class="container-fluid" id="corpoProva">
            <form method="post" action="api/V1/Gabarito/mostrar">
            <?php
                require_once 'API/V1/Classes/Gabarito.php';
                
                $gabarito = new Gabarito();
                $json_str = json_encode($gabarito->mostrar());
                
                $obj = json_decode($json_str);
                
                for( $i = 0; $i < count($obj); $i++ ){
                    $gabaritos[$i] = $obj[$i];               
                }
                
                foreach( $gabaritos as $e){
                    echo "<p>";                    
                    echo " <strong>ID Prova: </strong> $e->idGABARITO <br>";
                    echo " <strong>Questão 1:</strong> $e->Q1 ";
                    echo " <strong>Questão 2:</strong> $e->Q2 -";
                    echo " <strong>Questão 3:</strong> $e->Q3 -";
                    echo " <strong>Questão 4:</strong> $e->Q4 -";
                    echo " <strong>Questão 5:</strong> $e->Q5 -";
                    echo " <strong>Questão 6:</strong> $e->Q6 -";
                    echo " <strong>Questão 7:</strong> $e->Q7 -";
                    echo " <strong>Questão 8:</strong> $e->Q8 -";
                    echo " <strong>Questão 9:</strong> $e->Q9 -";
                    echo " <strong>Questão 10:</strong>$e->Q10 ";         
                    echo "</p>";
                    echo "<hr>";
                }
            ?>
                <input id="btnJson" type="submit" value="Mostrar JSON">
            </form>
        </div>
        <footer>
            <div class="container-fluid" style="text-align: center; padding: 10% 0%">
                <h3>Favor não utilizar esta aba para colar na prova !</h3>
            </div>
        </footer>
    </body>
</html>
