<html>    
    <head>        
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="CSS/Style.css" media="screen"/>
    </head>

    <?php
           #MATERIA
           $con = new PDO('mysql: host=localhost; dbname=mydb;', 'root', ''); 

           $sql = "SELECT * FROM MATERIA";
           $sql = $con->prepare($sql);
           $sql->execute();
           
           $result = array();
           
           while($rows= $sql->fetch(PDO::FETCH_ASSOC)){
               $result[] = $rows;                           
           }
           
           $json_str = json_encode($result);
           $obj = json_decode($json_str);
           
           for( $i = 0; $i < count($obj); $i++ ){
               $materia[$i] = $obj[$i];               
           } 
           
       ?>       
    
    <body>
        <div class="container-fluid" id="topo">  
            <h1 id="titulo">Escola / Gabarito</h1>
        </div>
            <!-- CHAMADA MÉTODOS ALUNOS  -->
            <div class="container-fluid" id="cadastroAlunos">
                <div class="col-sm-6">
                    <h1>Cadastro de alunos</h1><br>                                        
                    <form method="post" action="api/V1/Alunos/cadastrar">     
                        <input type="text" id="txtPadrao" name="fmatricula" placeholder="Matrícula" value=""><br><br>           
                        <input type="text" id="txtPadrao" name="fnome" placeholder="Nome Aluno" value=""><br><br>           
                        <input id="btnPadrao" type="submit" value="Cadastrar"><br><br>
                        <a href="Alunos.php">Mostrar Alunos</a><br>     
                    </form>                                 
                </div>
                <!-- CHAMADA MÉTODOS MATÉRIAS  -->
                <div class="col-sm-6">
                <h1>Cadastro de Matérias</h1><br>                                        
                    <form method="post" action="api/V1/Materia/cadastrar">     
                        <input type="text" id="txtPadrao" name="NomeMateria" placeholder="Matéria" value=""><br><br>           
                        <input type="text" id="txtPadrao" name="NomeProfessor" placeholder="Professor" value=""><br><br>           
                        <input id="btnPadrao" type="submit" value="Cadastrar"><br><br>      
                        <a href="Materia.php">Mostrar Matérias</a><br>
                    </form>   
                </div>
            </div>

        <!-- QUESTÕES GABARITO  -->
        <div class="container-fluid" id="corpoGabarito">
            <form method="post" action="api/V1/Gabarito/cadastrar" required>                   
            <!-- MATÉRIAS -->
            <div class="row" id="topo">
                <h1 id="titulo" >Cadastro de Gabarito</h1>                
                <?php 
                    echo"<select id='materia' name='materia'>";
                        echo"<option value=''>Selecione uma matéria</option>";
                        foreach ( $materia as $e ){ echo"<option value='$e->PKMATERIA'>$e->PKMATERIA - $e->NomeMateria</option>";}
                    echo"</select>";
                ?>    
            </div>
                <div id="questoes" class="col-sm-6">
                <p><b>Questão 1:</b>
                <input type="radio" id="Q1" name="Q1" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q1" name="Q1" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q1" name="Q1" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 2:</b>
                <input type="radio" id="Q2" name="Q2" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q2" name="Q2" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q2" name="Q2" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 3:</b>
                <input type="radio" id="Q3" name="Q3" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q3" name="Q3" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q3" name="Q3" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 4:</b>
                <input type="radio" id="Q4" name="Q4" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q4" name="Q4" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q4" name="Q4" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 5:</b>
                <input type="radio" id="Q5" name="Q5" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q5" name="Q5" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q5" name="Q5" value="3">
                <label >Opção 3</label>
                </p>
                </div>

                <div id="questoes" class="col-sm-6">
                <p><b>Questão 6:</b>
                <input type="radio" id="Q6" name="Q6" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q6" name="Q6" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q6" name="Q6" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 7:</b>
                <input type="radio" id="Q7" name="Q7" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q7" name="Q7" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q7" name="Q7" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 8:</b>
                <input type="radio" id="Q8" name="Q8" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q8" name="Q8" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q8" name="Q8" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 9:</b>
                <input type="radio" id="Q9" name="Q9" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q9" name="Q9" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q9" name="Q9" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 10:</b>
                <input type="radio" id="Q10" name="Q10" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q10" name="Q10" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q10" name="Q10" value="3">
                <label >Opção 3</label>
                </p>               
                </div> 
                <div class="col-sm-12">
                    <input type="text" id="txtPadrao" name="descricao" placeholder="Descrição" required>
                </div>                              
                <!-- CADASTRAR GABARITO -->
                <div class="col-sm-6">
                    <input id="btnPadrao" type="submit" value="Cadastrar" style="float: right">                    
                </div>
               
            </form>
            <!-- MOSTRAR GABARITO -->
            <div class="col-sm-6">
                <form method="post" action="Gabarito.php" style="float: left">
                    <input id="btnPadrao" type="submit" value="Mostrar"> 
                </form>             
            </div>
        </div> 
        
        <footer>
            <div>
            </div>
        </footer>
    </body>
</html>