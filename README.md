# Escola / Gabarito

**Requisitos obrigatórios:**

Crie uma API HTTP que permita à escola:

**-** Cadastrar o gabarito de cada prova;

**-** Cadastrar as respostas de cada aluno para cada prova;

**-** Verificar a nota final de cada aluno;

**-** Listar os alunos aprovados;

**Restrições**

**-** A nota total da prova é sempre maior que 0 e menor que 10.

**-** A quantidade máxima de alunos é 100.

**-** O peso de cada questão é sempre um inteiro maior que 0.

**-** Os alunos aprovados tem média de notas maior do que 7.

**-** A entrada e saída de dados deverá ser em JSON.

# Tecnologias Utilizadas

**Visual Code:** Editor para desenvolvimento da aplicação;

**MySQL Workbench:** Para criação da modelagem da aplicação;

**WampServer:** Para criação do servidor;

**GitLab:** Para controle pessoal de organização dos fontes;

# Observações

- Commitado arquivo bd.mwb para montagem de ambiente e de testes. 

- Quando executado cadastro de algum registro, necessário executar "ctrl + F5" para limpeza de cache, Não foi alterado este padrão pois, achei interessante deixar vísivel o retorno do JSON.








