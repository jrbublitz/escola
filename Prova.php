<html>    
    <head>        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" type="text/css" href="CSS/Style.css" media="screen" />        
    </head>

    <?php
        #ALUNO
        $matricula = $_POST['matricula'];        
        
        $con = new PDO('mysql: host=localhost; dbname=mydb;', 'root', ''); 
        
        $sql = "SELECT * FROM ALUNO WHERE MATRICULA = $matricula";
        $sql = $con->prepare($sql);
        $sql->execute();
        
        $resultados = array();
        
        while($row = $sql->fetch(PDO::FETCH_ASSOC)){
            $resultados[] = $row;                           
        }
        
        if(!$resultados){
            echo "<h1>Não existem alunos</h1>";
            throw new Exception("Aluno não encontrado", 1);       
        }   
        
        $json_str = json_encode($resultados);
        $obj = json_decode($json_str);
        
        for( $i = 0; $i < count($obj); $i++ ){
            $aluno[$i] = $obj[$i];               
        } 
        
        #MATERIA
        $sql = "SELECT * FROM MATERIA";
        $sql = $con->prepare($sql);
        $sql->execute();
        
        $result = array();
        
        while($rows= $sql->fetch(PDO::FETCH_ASSOC)){
            $result[] = $rows;                           
        }
        
        $json_str = json_encode($result);
        $obj = json_decode($json_str);
        
        for( $i = 0; $i < count($obj); $i++ ){
            $materia[$i] = $obj[$i];               
        } 

        #GABIRATO
        $sql = "Select * from gabarito A";
        $sql = $con->prepare($sql);
        $sql->execute();
        
        $res = array();
        
        while($rows= $sql->fetch(PDO::FETCH_ASSOC)){
            $res[] = $rows;                           
        }
        
        $json_str = json_encode($res);
        $obj = json_decode($json_str);
        
        for( $i = 0; $i < count($obj); $i++ ){
            $gabarito[$i] = $obj[$i];               
        }
        
        #PROVA ALUNO
        $prova = null;
        $sql = "SELECT A.Q1 AS 'PQ1', A.Q2 AS 'PQ2', A.Q3 AS 'PQ3', A.Q4 AS 'PQ4', A.Q5 AS 'PQ5',
        A.Q6 AS 'PQ6', A.Q7 AS 'PQ7', A.Q8 AS 'PQ8', A.Q9 AS 'PQ9', A.Q10 AS 'PQ10',
         A.FKAluno, A.FKMateria, A.PKProva,
         B.*,
         C.Descricao as descricao, C.*,
         D.*
         FROM PROVA A 
         INNER JOIN MATERIA B on A.FKMateria = B.PKMATERIA
         INNER JOIN GABARITO C ON C.idGABARITO = A.FKGabarito
         INNER JOIN aluno D on D.Matricula = A.FKAluno
         WHERE A.FKALUNO = $matricula";

        $sql = $con->prepare($sql);
        $sql->execute();
        
        $resulti = array();
        
        while($row = $sql->fetch(PDO::FETCH_ASSOC)){
            $resulti[] = $row;                           
        }        
        
        $json_str = json_encode($resulti);
        $obj = json_decode($json_str);
        
        for( $i = 0; $i < count($obj); $i++ ){
            $prova[$i] = $obj[$i];               
        }
    ?>
    
    <body>      
        <div class="container-fluid" id="corpoProva">
            <form method="post" action="api/v1/Prova/mostrar">                       
                <div class="row" id="topoProva">                 
                    <?php
                        foreach ( $aluno as $e ){
                            echo "<h1 id='titulo'>Prova de: $e->Nome</h1>";
                            echo "<input type='hidden' id='matricula' name='matricula' value='$e->Matricula'>";                            
                        } 
                        
                        echo"<select id='materia' name='materia'>";
                            echo"<option value=''>Selecione uma matéria</option>";
                            foreach ( $materia as $e ){ echo"<option value='$e->PKMATERIA'>$e->PKMATERIA - $e->NomeMateria</option>";}
                        echo"</select>";

                       
                    ?>
                    <select id='gabarito' name='gabarito'>
                    <script>
                        $("#materia").on("change",function(){
                            var idMateria = $("#materia").val();
                            
                            $.ajax({
                               url: 'PegaGabarito.php',
                               type: 'POST',
                               data: {id:idMateria},
                               beforeSend: function(){
                                   $("#gabarito").html("Carregando...");
                               },
                                
                               success: function(data){
                                   $("#gabarito").html(data);
                               },
                                
                               Error: function(data){
                                   $("#gabarito").html("ERROR");     
                               }
                            });
                        });
                        </script>                            
                    </select>
                </div>
                <div class="col-sm-6">
                <p><b>Questão 1:</b>
                <input type="radio" id="Q1" name="Q1" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q1" name="Q1" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q1" name="Q1" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 2:</b>
                <input type="radio" id="Q2" name="Q2" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q2" name="Q2" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q2" name="Q2" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 3:</b>
                <input type="radio" id="Q3" name="Q3" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q3" name="Q3" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q3" name="Q3" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 4:</b>
                <input type="radio" id="Q4" name="Q4" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q4" name="Q4" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q4" name="Q4" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 5:</b>
                <input type="radio" id="Q5" name="Q5" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q5" name="Q5" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q5" name="Q5" value="3">
                <label >Opção 3</label>
                </p>
                </div>

                <div class="col-sm-6">
                <p><b>Questão 6:</b>
                <input type="radio" id="Q6" name="Q6" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q6" name="Q6" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q6" name="Q6" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 7:</b>
                <input type="radio" id="Q7" name="Q7" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q7" name="Q7" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q7" name="Q7" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 8:</b>
                <input type="radio" id="Q8" name="Q8" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q8" name="Q8" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q8" name="Q8" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 9:</b>
                <input type="radio" id="Q9" name="Q9" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q9" name="Q9" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q9" name="Q9" value="3">
                <label >Opção 3</label>
                </p>

                <p><b>Questão 10:</b>
                <input type="radio" id="Q10" name="Q10" value="1">
                <label >Opção 1</label>
                <input type="radio" id="Q10" name="Q10" value="2">
                <label >Opção 2</label>
                <input type="radio" id="Q10" name="Q10" value="3">
                <label >Opção 3</label>
                </p>
                </div>                                        
                <input id="btnPadrao" type="submit" value="Cadastrar">    
            </form>       
        </div>                                  
        <hr> 
        <div class="container-fluid" style="text-align: center">
            <h2>Prova já realizadas</h2>
            <table style="width:100%">
                <tr>
                    <th>Matérias</th>
                    <th>Prova</th>
                    <th>Q1</th> 
                    <th>Q2</th> 
                    <th>Q3</th>
                    <th>Q4</th>
                    <th>Q5</th>
                    <th>Q6</th>
                    <th>Q7</th>
                    <th>Q8</th>
                    <th>Q9</th>
                    <th>Q10</th>
                    <th>Nota</th>
                    <th>Situação</th>
                </tr>                                
                <?php                
                if(!is_null($prova)){                
                    foreach ( $prova as $e ){                        
                        $situacao = "";
                        $nota = 0.00;
                        $corNota = '';                        
                        echo "<tr>";                        
                            echo "<td>$e->NomeMateria</td>";
                            echo "<td>$e->descricao</td>";
                            echo "<td>$e->PQ1</td>"; if($e->PQ1==$e->Q1){$nota = $nota+1;}
                            echo "<td>$e->PQ2</td>"; if($e->PQ2==$e->Q2){$nota = $nota+1;}   
                            echo "<td>$e->PQ3</td>"; if($e->PQ3==$e->Q3){$nota = $nota+1;}   
                            echo "<td>$e->PQ4</td>"; if($e->PQ4==$e->Q4){$nota = $nota+1;}
                            echo "<td>$e->PQ5</td>"; if($e->PQ5==$e->Q5){$nota = $nota+1;}
                            echo "<td>$e->PQ6</td>"; if($e->PQ6==$e->Q6){$nota = $nota+1;}
                            echo "<td>$e->PQ7</td>"; if($e->PQ7==$e->Q7){$nota = $nota+1;}
                            echo "<td>$e->PQ8</td>"; if($e->PQ8==$e->Q8){$nota = $nota+1;}
                            echo "<td>$e->PQ9</td>"; if($e->PQ9==$e->Q9){$nota = $nota+1;}
                            echo "<td>$e->PQ10</td>";if($e->PQ10==$e->Q10){$nota = $nota+1;}                            
                            echo "<td>$nota</td>";                            
                            include 'AtualizaSituacao.php';
                            if($nota >= 7){                                
                                    AtualizaSituacao($e->PKProva,'Aprovado');                                
                                    $situacao ='Aprovado';
                            }else{
                                AtualizaSituacao($e->PKProva,'Reprovado');
                                $situacao ='Reprovado';
                            }
                            echo "<td>$situacao</td>";                            
                        echo "</tr>";                                                
                    }
                }
                ?>                                                        
            </table>            
        </div>                   
        <hr>         
        <div class="container-fluid" style="text-align: center">
            <form method="post" action="api/V1/Prova/mostrarJson">                
                <input id="btnJson" type="submit" value="Mostrar JSON">
            </form>

            <footer>
                <h2>Boa sorte na prova !</h2>
            </footer>
        </div>
    </body>
</html>